﻿namespace ProjectStructure.Domain.Enums
{
    public enum TaskState : byte
    {
        Planned = 0,
        Started,
        Finished,
        Cancelled
    }
}
