﻿using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.Domain.Interfaces.Repositories
{
    public interface IWriteRepository<TEntity> where TEntity : class, IEntity
    {
        public IQueryable<TEntity> WriteQuery();

        public TEntity Add(TEntity entity);

        public TEntity Delete(TEntity entity);

        public Task<int> SaveChangesAsync();
    }
}
