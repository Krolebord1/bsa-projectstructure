﻿using System;
using ProjectStructure.Domain.Enums;
using ProjectStructure.Domain.Interfaces;

namespace ProjectStructure.Domain.Entities
{
    public class UserTask : IEntity
    {
        public int Id { get; set; }

        public string Name { get; set; } = string.Empty;

        public string Description { get; set; } = string.Empty;

        public TaskState State { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset? FinishedAt { get; set; }

        public int? ProjectId { get; set; }
        public Project? Project { get; set; } = default!;

        public int? PerformerId { get; set; }
        public User? Performer { get; set; }
    }
}
