﻿using FluentValidation;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.Validation.CustomValidators;

namespace ProjectStructure.ApplicationServices.Validation
{
    public class UserWriteDTOValidator : AbstractValidator<UserWriteDTO>
    {
        public UserWriteDTOValidator()
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .EmailAddress();

            RuleFor(x => x.FirstName)
                .NotEmpty()
                .Name();

            RuleFor(x => x.LastName)
                .NotNull();
        }
    }
}
