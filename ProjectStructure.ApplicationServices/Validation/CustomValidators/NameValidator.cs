﻿using FluentValidation;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.ApplicationServices.Validation.CustomValidators
{
    public static class NameValidator
    {
        public static IRuleBuilder<T, string> Name<T>(this IRuleBuilder<T, string> rule) =>
            rule.MaximumLength(EntityConstants.MaxNameLength);
    }
}
