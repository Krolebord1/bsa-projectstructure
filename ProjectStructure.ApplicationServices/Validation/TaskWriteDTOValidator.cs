﻿using FluentValidation;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.Validation.CustomValidators;

namespace ProjectStructure.ApplicationServices.Validation
{
    public class TaskWriteDTOValidator : AbstractValidator<TaskWriteDTO>
    {
        public TaskWriteDTOValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Name();

            RuleFor(x => x.Description)
                .NotNull();
        }
    }
}
