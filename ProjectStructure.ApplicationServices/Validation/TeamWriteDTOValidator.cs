﻿using FluentValidation;
using ProjectStructure.ApplicationServices.DTOs.Teams;
using ProjectStructure.ApplicationServices.Validation.CustomValidators;

namespace ProjectStructure.ApplicationServices.Validation
{
    public class TeamWriteDTOValidator : AbstractValidator<TeamWriteDTO>
    {
        public TeamWriteDTOValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Name();
        }
    }
}
