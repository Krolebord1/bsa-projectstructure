﻿using FluentValidation;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.Validation.CustomValidators;

namespace ProjectStructure.ApplicationServices.Validation
{
    public class ProjectWriteDTOValidator : AbstractValidator<ProjectWriteDTO>
    {
        public ProjectWriteDTOValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .Name();

            RuleFor(x => x.Description)
                .NotNull();
        }
    }
}
