﻿using Microsoft.Extensions.DependencyInjection;
using ProjectStructure.Domain.Interfaces;

namespace ProjectStructure.ApplicationServices
{
    public static class RegisterServices
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddSingleton<IDateProvider, DateProvider>();

            return services;
        }
    }
}
