﻿using System;
using ProjectStructure.Domain.Enums;

namespace ProjectStructure.ApplicationServices.DTOs.Tasks
{
    public record TaskWriteDTO(
        int ProjectId,
        int? PerformerId,
        string Name,
        string Description,
        TaskState State,
        DateTimeOffset? FinishedAt
    );
}
