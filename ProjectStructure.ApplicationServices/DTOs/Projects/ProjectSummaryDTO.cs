﻿using ProjectStructure.ApplicationServices.DTOs.Tasks;

namespace ProjectStructure.ApplicationServices.DTOs.Projects
{
    public record ProjectSummaryDTO(
        ProjectReadDTO Project,
        TaskReadDTO? LongestTask,
        TaskReadDTO? ShortestTask,
        int? UsersCount
    );
}
