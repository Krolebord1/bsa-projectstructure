﻿using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.DTOs.Tasks;

namespace ProjectStructure.ApplicationServices.DTOs
{
    public record UserSummaryDTO(
        UserReadDTO User,
        ProjectReadDTO? LastProject,
        int LastProjectTaskCount,
        int UnfinishedTasksCount,
        TaskReadDTO? LongestFinishedTask
    );
}
