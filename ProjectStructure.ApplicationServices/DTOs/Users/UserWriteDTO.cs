﻿using System;

namespace ProjectStructure.ApplicationServices.DTOs
{
    public record UserWriteDTO(
        int? TeamId,
        string FirstName,
        string LastName,
        string Email,
        DateTimeOffset BirthDay
    );
}
