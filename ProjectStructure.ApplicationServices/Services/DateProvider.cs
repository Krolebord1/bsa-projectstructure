﻿using System;
using ProjectStructure.Domain.Interfaces;

namespace ProjectStructure.ApplicationServices
{
    public class DateProvider : IDateProvider
    {
        public DateTimeOffset Now => DateTimeOffset.Now;
    }
}
