﻿using System.Collections.Generic;
using MediatR;
using ProjectStructure.ApplicationServices.DTOs.Projects;

namespace ProjectStructure.ApplicationServices.Queries.Selection
{
    public class GetUserTaskCountQuery : IRequest<IEnumerable<KeyValuePair<ProjectReadDTO, int>>>
    {
        public int UserId { get; }

        public GetUserTaskCountQuery(int userId)
        {
            UserId = userId;
        }
    }
}
