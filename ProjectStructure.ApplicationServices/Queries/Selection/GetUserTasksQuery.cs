﻿using System.Collections.Generic;
using MediatR;
using ProjectStructure.ApplicationServices.DTOs.Tasks;

namespace ProjectStructure.ApplicationServices.Queries.Selection
{
    public class GetUserTasksQuery : IRequest<IEnumerable<TaskReadDTO>>
    {
        public int UserId { get; }

        public int MaxTaskNameLength => 45;

        public GetUserTasksQuery(int userId)
        {
            UserId = userId;
        }
    }
}
