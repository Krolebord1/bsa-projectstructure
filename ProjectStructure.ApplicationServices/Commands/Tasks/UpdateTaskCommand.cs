﻿using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.DTOs.Tasks;

namespace ProjectStructure.ApplicationServices.Commands.Tasks
{
    public class UpdateTaskCommand : IRequest<OneOf<Success, NotFound>>
    {
        public int Id { get; }

        public TaskWriteDTO TaskDTO { get; }

        public UpdateTaskCommand(int id, TaskWriteDTO taskDTO)
        {
            Id = id;
            TaskDTO = taskDTO;
        }
    }
}
