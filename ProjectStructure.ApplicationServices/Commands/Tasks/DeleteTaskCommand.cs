﻿using MediatR;
using OneOf;
using OneOf.Types;

namespace ProjectStructure.ApplicationServices.Commands.Tasks
{
    public class DeleteTaskCommand : IRequest<OneOf<Success, NotFound>>
    {
        public int Id { get; }

        public DeleteTaskCommand(int id)
        {
            Id = id;
        }
    }
}
