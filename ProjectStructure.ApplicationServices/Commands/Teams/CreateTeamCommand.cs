﻿using MediatR;
using ProjectStructure.ApplicationServices.DTOs.Teams;

namespace ProjectStructure.ApplicationServices.Commands.Teams
{
    public class CreateTeamCommand : IRequest
    {
        public TeamWriteDTO TeamDTO { get; }

        public CreateTeamCommand(TeamWriteDTO teamDTO)
        {
            TeamDTO = teamDTO;
        }
    }
}
