﻿using MediatR;
using OneOf;
using OneOf.Types;

namespace ProjectStructure.ApplicationServices.Commands.Teams
{
    public class DeleteTeamCommand : IRequest<OneOf<Success, NotFound>>
    {
        public int Id { get; }

        public DeleteTeamCommand(int id)
        {
            Id = id;
        }
    }
}
