﻿using MediatR;
using OneOf;
using OneOf.Types;

namespace ProjectStructure.ApplicationServices.Commands.Users
{
    public class DeleteUserCommand : IRequest<OneOf<Success, NotFound>>
    {
        public int Id { get; }

        public DeleteUserCommand(int id)
        {
            Id = id;
        }
    }
}
