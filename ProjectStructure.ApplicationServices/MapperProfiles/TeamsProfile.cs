﻿using AutoMapper;
using ProjectStructure.ApplicationServices.DTOs.Teams;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.ApplicationServices.MapperProfiles
{
    public class TeamsProfile : Profile
    {
        public TeamsProfile()
        {
            CreateMap<Team, TeamReadDTO>().ReverseMap();
            CreateMap<TeamWriteDTO, Team>();
        }
    }
}
