﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using ProjectStructure.ApplicationServices.Commands.Tasks;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Tasks
{
    public class CreateTaskHandler : IRequestHandler<CreateTaskCommand>
    {
        private readonly IRepository<UserTask> _tasksRepository;
        private readonly IMapper _mapper;
        private readonly IDateProvider _dateProvider;

        public CreateTaskHandler(IRepository<UserTask> tasksRepository, IMapper mapper, IDateProvider dateProvider)
        {
            _tasksRepository = tasksRepository;
            _mapper = mapper;
            _dateProvider = dateProvider;
        }

        public async Task<Unit> Handle(CreateTaskCommand request, CancellationToken cancellationToken)
        {
            var task = _mapper.Map<TaskWriteDTO, UserTask>(request.TaskDTO);

            task.CreatedAt = _dateProvider.Now;

            _tasksRepository.Add(task);

            await _tasksRepository.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
