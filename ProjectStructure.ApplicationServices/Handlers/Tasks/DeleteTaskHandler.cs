﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.Commands.Tasks;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Tasks
{
    public class DeleteTaskHandler : IRequestHandler<DeleteTaskCommand, OneOf<Success, NotFound>>
    {
        private readonly IRepository<UserTask> _tasksRepository;

        public DeleteTaskHandler(IRepository<UserTask> tasksRepository)
        {
            _tasksRepository = tasksRepository;
        }

        public async Task<OneOf<Success, NotFound>> Handle(DeleteTaskCommand request, CancellationToken cancellationToken)
        {
            var task = await _tasksRepository.ReadAsync(request.Id);

            if (task == null)
                return new NotFound();

            _tasksRepository.Delete(task);

            await _tasksRepository.SaveChangesAsync();

            return new Success();
        }
    }
}
