﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.Queries.Selection;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Selection
{
    public class GetProjectSummariesHandler : IRequestHandler<GetProjectSummariesQuery, IEnumerable<ProjectSummaryDTO>>
    {
        private readonly IReadRepository<Project> _projectsRepository;
        private readonly IMapper _mapper;

        public GetProjectSummariesHandler(IReadRepository<Project> projectsRepository, IMapper mapper)
        {
            _projectsRepository = projectsRepository;
            _mapper = mapper;
        }

        public async Task<IEnumerable<ProjectSummaryDTO>> Handle(GetProjectSummariesQuery request, CancellationToken cancellationToken)
        {
            var projects = await _projectsRepository.ReadQuery()
                .Include(x => x.Tasks)
                .Include(x => x.Team)
                .ToListAsync(cancellationToken);

            if (cancellationToken.IsCancellationRequested)
                return new List<ProjectSummaryDTO>();

            return projects
                .Select(project =>
                {
                    UserTask? longestTask = !project.Tasks.Any() ? null :
                        project.Tasks.Aggregate(
                            (taskA, taskB) => taskA.Description.Length > taskB.Description.Length ? taskA : taskB);

                    UserTask? shortestTask = !project.Tasks.Any() ? null :
                        project.Tasks.Aggregate(
                            (taskA, taskB) => taskA.Name.Length < taskB.Name.Length ? taskA : taskB);

                    int? teamCount =
                        project.Description.Length > request.MinProjectDescriptionLength ||
                        project.Tasks.Count < request.MaxProjectTasksCount
                            ? project.Team?.Users.Count
                            : null;

                    return new ProjectSummaryDTO(
                        _mapper.Map<Project, ProjectReadDTO>(project),
                        longestTask != null ? _mapper.Map<UserTask, TaskReadDTO>(longestTask) : null,
                        shortestTask != null ? _mapper.Map<UserTask, TaskReadDTO>(shortestTask) : null,
                        teamCount
                    );
                });
        }
    }
}
