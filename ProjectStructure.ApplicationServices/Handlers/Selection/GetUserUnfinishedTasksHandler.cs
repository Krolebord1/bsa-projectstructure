﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.Queries.Selection;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Selection
{
    public class GetUserUnfinishedTasksHandler : IRequestHandler<GetUserUnfinishedTasksQuery, OneOf<IEnumerable<TaskReadDTO>, NotFound>>
    {
        private readonly IReadRepository<User> _usersRepository;
        private readonly IMapper _mapper;

        public GetUserUnfinishedTasksHandler(IReadRepository<User> usersRepository, IMapper mapper)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
        }

        public async Task<OneOf<IEnumerable<TaskReadDTO>, NotFound>> Handle(GetUserUnfinishedTasksQuery request, CancellationToken cancellationToken)
        {
            var user = await _usersRepository.ReadQuery()
                .Where(x => x.Id == request.UserId)
                .Include(x => x.Tasks)
                .FirstOrDefaultAsync(cancellationToken);

            if (cancellationToken.IsCancellationRequested || user == null)
                return new NotFound();

            var users = user.Tasks
                .Where(task => task.FinishedAt == null)
                .Select(task => _mapper.Map<TaskReadDTO>(task));

            return OneOf<IEnumerable<TaskReadDTO>, NotFound>.FromT0(users);
        }
    }
}
