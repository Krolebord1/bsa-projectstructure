﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using ProjectStructure.ApplicationServices.Commands.Projects;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Projects
{
    public class CreateProjectHandler : IRequestHandler<CreateProjectCommand>
    {
        private readonly IRepository<Project> _projectsRepository;
        private readonly IMapper _mapper;
        private readonly IDateProvider _dateProvider;

        public CreateProjectHandler(IRepository<Project> projectsRepository, IMapper mapper, IDateProvider dateProvider)
        {
            _projectsRepository = projectsRepository;
            _mapper = mapper;
            _dateProvider = dateProvider;
        }

        public async Task<Unit> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
        {
            var project = _mapper.Map<ProjectWriteDTO, Project>(request.ProjectDTO);

            project.CreatedAt = _dateProvider.Now;

            _projectsRepository.Add(project);

            await _projectsRepository.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
