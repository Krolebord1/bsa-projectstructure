﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.Commands.Teams;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Teams
{
    public class DeleteTeamHandler : IRequestHandler<DeleteTeamCommand, OneOf<Success, NotFound>>
    {
        private readonly IRepository<Team> _teamsRepository;

        public DeleteTeamHandler(IRepository<Team> teamsRepository)
        {
            _teamsRepository = teamsRepository;
        }

        public async Task<OneOf<Success, NotFound>> Handle(DeleteTeamCommand request, CancellationToken cancellationToken)
        {
            var team = await _teamsRepository.ReadAsync(request.Id);

            if (team == null)
                return new NotFound();

            _teamsRepository.Delete(team);

            await _teamsRepository.SaveChangesAsync();

            return new Success();
        }
    }
}
