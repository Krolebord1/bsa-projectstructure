﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.Commands.Teams;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Teams
{
    public class UpdateTeamHandler : IRequestHandler<UpdateTeamCommand, OneOf<Success, NotFound>>
    {
    private readonly IRepository<Team> _teamsRepository;
    private readonly IMapper _mapper;

    public UpdateTeamHandler(IRepository<Team> teamsRepository, IMapper mapper)
    {
        _teamsRepository = teamsRepository;
        _mapper = mapper;
    }

    public async Task<OneOf<Success, NotFound>> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
    {
        var team = await _teamsRepository.ReadAsync(request.Id);

        if (team == null)
            return new NotFound();

        _mapper.Map(request.TeamDTO, team);

        await _teamsRepository.SaveChangesAsync();

        return new Success();
    }
    }
}
