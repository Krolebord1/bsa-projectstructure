﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using ProjectStructure.ApplicationServices.Commands.Users;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Users
{
    public class CreateUserHandler : IRequestHandler<CreateUserCommand>
    {
        private readonly IRepository<User> _usersRepository;
        private readonly IMapper _mapper;
        private readonly IDateProvider _dateProvider;

        public CreateUserHandler(IRepository<User> usersRepository, IMapper mapper, IDateProvider dateProvider)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
            _dateProvider = dateProvider;
        }

        public async Task<Unit> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var user = _mapper.Map<UserWriteDTO, User>(request.UserDTO);

            user.RegisteredAt = _dateProvider.Now;

            _usersRepository.Add(user);

            await _usersRepository.SaveChangesAsync();

            return Unit.Value;
        }
    }
}
