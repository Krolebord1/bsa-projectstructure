﻿using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.Commands.Users;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.ApplicationServices.Handlers.Users
{
    public class UpdateUserHandler : IRequestHandler<UpdateUserCommand, OneOf<Success, NotFound>>
    {
        private readonly IRepository<User> _usersRepository;
        private readonly IMapper _mapper;

        public UpdateUserHandler(IRepository<User> usersRepository, IMapper mapper)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
        }

        public async Task<OneOf<Success, NotFound>> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _usersRepository.GetAsync(request.Id);

            if (user == null)
                return new NotFound();

            _mapper.Map(request.UserDto, user);

            await _usersRepository.SaveChangesAsync();

            return new Success();
        }
    }
}
