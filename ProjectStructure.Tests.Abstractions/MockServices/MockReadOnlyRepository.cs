﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MockQueryable.FakeItEasy;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.Tests.Abstractions.MockServices
{
    public class MockReadOnlyRepository<TEntity> : IReadRepository<TEntity>
        where TEntity : class, IEntity {

        protected IList<TEntity> entities;

        public MockReadOnlyRepository()
        {
            entities = new List<TEntity>();
        }

        public MockReadOnlyRepository(IList<TEntity> entities)
        {
            this.entities = entities;
        }

        public IQueryable<TEntity> ReadQuery() =>
            entities.AsQueryable().BuildMock();

        public Task<TEntity?> ReadAsync(int id) =>
            Task.FromResult(entities.FirstOrDefault(entity => entity.Id == id));

        public Task<IList<TEntity>> ReadAllAsync() =>
            Task.FromResult(entities);
    }
}
