﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using Xunit;

namespace ProjectStructure.Tests.Integration.Controllers
{
    public class SelectionsControllerTests : IClassFixture<AppFactory>
    {
        private readonly HttpClient _client;

        public SelectionsControllerTests(AppFactory factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task GetUserSummaryWithValidId_RespondsValidUserSummaryDTO()
        {
            int id = 1;
            var response = await _client.GetAsync($"api/selections/users/{id}/summary");

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var json = await response.Content.ReadAsStringAsync();
            var summary = JsonConvert.DeserializeObject<UserSummaryDTO>(json);

            Assert.NotNull(summary);
            Assert.NotNull(summary!.User);
        }

        [Fact]
        public async Task GetUserSummaryWithInvalidId()
        {
            int id = 0;
            var response = await _client.GetAsync($"api/selections/users/{id}/summary");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetUnfinishedUserTasksWithValidId()
        {
            int id = 1;
            var response = await _client.GetAsync($"api/selections/users/{id}/tasks/unfinished");

            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            var json = await response.Content.ReadAsStringAsync();
            var tasks = JsonConvert.DeserializeObject<IEnumerable<TaskReadDTO>>(json);

            Assert.NotNull(tasks);
        }

        [Fact]
        public async Task GetUnfinishedUserTasksWithInvalidId()
        {
            int id = 0;
            var response = await _client.GetAsync($"api/selections/users/{id}/tasks/unfinished");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
