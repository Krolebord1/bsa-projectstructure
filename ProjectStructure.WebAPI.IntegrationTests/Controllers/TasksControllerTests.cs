﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.Tests.Integration.Controllers
{
    public class TasksControllerTests : IClassFixture<AppFactory>
    {
        private readonly HttpClient _client;

        public TasksControllerTests(AppFactory factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task GetTaskWithCode200_ThenRemoveTaskWithCode200_ThenGetTaskWithCode404()
        {
            var id = 1;
            var uri = $"api/tasks/{id}";

            var response = await _client.GetAsync(uri);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            response = await _client.DeleteAsync(uri);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            response = await _client.GetAsync(uri);
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task ThenRemoveInvalidTaskWithCode404()
        {
            var id = -1;
            var uri = $"api/tasks/{id}";

            var response = await _client.DeleteAsync(uri);
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
