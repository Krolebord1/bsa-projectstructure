﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace ProjectStructure.Tests.Integration.Controllers
{
    public class UsersControllerTests : IClassFixture<AppFactory>
    {
        private readonly HttpClient _client;

        public UsersControllerTests(AppFactory factory)
        {
            _client = factory.CreateClient();
        }

        [Fact]
        public async Task GetUserWithCode200_ThenRemoveUserWithCode200_ThenGetUserWithCode404()
        {
            var id = 1;
            var uri = $"api/projects/{id}";

            var response = await _client.GetAsync(uri);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            response = await _client.DeleteAsync(uri);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            response = await _client.GetAsync(uri);
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task ThenRemoveInvalidUserWithCode404()
        {
            var id = -1;
            var uri = $"api/projects/{id}";

            var response = await _client.DeleteAsync(uri);
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
