﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.ApplicationServices.Commands.Users;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.Queries;

namespace ProjectStructure.WebAPI.Controllers
{
    [ApiController]
    [Route(APIRoutes.UsersController)]
    public class UsersController : ControllerBase
    {
        private readonly IMediator _mediator;

        public UsersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUsers()
        {
            var request = new GetEntitiesQuery<UserReadDTO>();
            var response = await _mediator.Send(request);

            return Ok(response);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<UserReadDTO>> GetUserById([FromRoute] int id)
        {
            var request = new GetEntityByIdQuery<UserReadDTO>(id);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult<UserReadDTO>>(
                user => Ok(user),
                notFound => NotFound()
            );
        }

        [HttpPost]
        public async Task<ActionResult> CreateUser([FromBody] UserWriteDTO userDto)
        {
            var request = new CreateUserCommand(userDto);
            await _mediator.Send(request);

            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> UpdateUser([FromRoute] int id, [FromBody] UserWriteDTO userDto)
        {
            var request = new UpdateUserCommand(id, userDto);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult>(
                success => NoContent(),
                notFound => NotFound()
            );
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteUser([FromRoute] int id)
        {
            var request = new DeleteUserCommand(id);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult>(
                success => Ok(),
                notFound => NotFound()
            );
        }
    }
}
