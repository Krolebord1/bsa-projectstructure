﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProjectStructure.ApplicationServices.Commands.Tasks;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.Queries;

namespace ProjectStructure.WebAPI.Controllers
{
    [ApiController]
    [Route(APIRoutes.TasksController)]
    public class TasksController : ControllerBase
    {
        private readonly IMediator _mediator;

        public TasksController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TaskReadDTO>>> GetTasks()
        {
            var request = new GetEntitiesQuery<TaskReadDTO>();
            var response = await _mediator.Send(request);

            return Ok(response);
        }

        [HttpGet("{id:int}")]
        public async Task<ActionResult<TaskReadDTO>> GetTaskById([FromRoute] int id)
        {
            var request = new GetEntityByIdQuery<TaskReadDTO>(id);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult<TaskReadDTO>>(
                task => Ok(task),
                notFound => NotFound()
            );
        }

        [HttpPost]
        public async Task<ActionResult> CreateTask([FromBody] TaskWriteDTO taskDto)
        {
            var request = new CreateTaskCommand(taskDto);
            await _mediator.Send(request);

            return StatusCode(StatusCodes.Status201Created);
        }

        [HttpPut("{id:int}")]
        public async Task<ActionResult> UpdateTask([FromRoute] int id, [FromBody] TaskWriteDTO taskDto)
        {
            var request = new UpdateTaskCommand(id, taskDto);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult>(
                success => NoContent(),
                notFound => NotFound()
            );
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult> DeleteTask([FromRoute] int id)
        {
            var request = new DeleteTaskCommand(id);
            var response = await _mediator.Send(request);

            return response.Match<ActionResult>(
                success => Ok(),
                notFound => NotFound()
            );
        }
    }
}
