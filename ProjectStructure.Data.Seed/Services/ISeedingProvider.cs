﻿using System.Collections.Generic;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.DTOs.Teams;

namespace ProjectStructure.Data.Seed.Services
{
    public interface ISeedingProvider
    {
        public IList<UserReadDTO> GetUsers();

        public IList<ProjectReadDTO> GetProjects();

        public IList<TeamReadDTO> GetTeams();

        public IList<TaskReadDTO> GetTasks();
    }
}
