﻿using System.IO;
using Newtonsoft.Json;

namespace ProjectStructure.Data.Seed.Services
{
    public class DataLoader : IDataLoader
    {
        public T GetDeserialized<T>(string path)
        {
            var jsonPath = Path.Combine(Path.GetDirectoryName(typeof(DataLoader).Assembly.Location)!, path);

            return JsonConvert.DeserializeObject<T>(File.ReadAllText(jsonPath))!;
        }
    }
}
