﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectStructure.Data.EntityConfigurationExtensions;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.Data.EntityConfigurations
{
    public class TasksConfiguration : IEntityTypeConfiguration<UserTask>
    {
        public void Configure(EntityTypeBuilder<UserTask> builder)
        {
            builder.ToTable("Tasks");

            builder.HasKey(task => task.Id);

            builder.Property(task => task.Name)
                .IsName();

            builder.Property(task => task.Description)
                .IsNonNullableString();

            builder
                .HasOne(task => task.Project)
                .WithMany(project => project!.Tasks)
                .HasForeignKey(task => task.ProjectId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);

            builder
                .HasOne(task => task.Performer)
                .WithMany(user => user!.Tasks)
                .HasForeignKey(task => task.PerformerId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
