﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProjectStructure.Data.EntityConfigurationExtensions;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.Data.EntityConfigurations
{
    public class UsersConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.HasKey(user => user.Id);

            builder.Property(user => user.FirstName)
                .IsName();

            builder.Property(user => user.Email)
                .IsRequired();

            builder.Property(user => user.LastName)
                .IsNonNullableString();

            builder
                .HasOne(user => user.Team)
                .WithMany(team => team!.Users)
                .HasForeignKey(user => user.TeamId)
                .IsRequired(false)
                .OnDelete(DeleteBehavior.SetNull);
        }
    }
}
