﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;

namespace ProjectStructure.Data.Repositories
{
    public class ReadOnlyRepository<TEntity> : IReadRepository<TEntity>
        where TEntity : class, IEntity
    {
        protected DbSet<TEntity> Entities { get; }

        public ReadOnlyRepository(AppContext context)
        {
            Entities = context.Set<TEntity>();
        }

        public IQueryable<TEntity> ReadQuery() =>
            Entities.AsNoTrackingWithIdentityResolution().AsQueryable();

        public async Task<TEntity?> ReadAsync(int id)
        {
            return await ReadQuery().FirstOrDefaultAsync(entity => entity.Id == id);
        }

        public async Task<IList<TEntity>> ReadAllAsync()
        {
            return await ReadQuery().ToListAsync();
        }
    }
}
