﻿using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.Domain.Entities;

namespace ProjectStructure.ConsoleClient.Extensions
{
    public static class ProjectSummaryDOTExtensions
    {
        public static string ToFotmattedString(this ProjectSummaryDTO dto) =>
            $"Project: {dto.Project.Name}" +
            $"\n\tLongest task: {dto.LongestTask?.Name ?? "none"}" +
            $"\n\tShortest task: {dto.ShortestTask?.Name ?? "none"}" +
            (dto.UsersCount != null ? $"\n\tUsers count: {dto.UsersCount.ToString()}" : string.Empty);
    }
}
