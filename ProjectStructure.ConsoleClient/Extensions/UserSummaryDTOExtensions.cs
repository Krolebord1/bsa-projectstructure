﻿using ProjectStructure.ApplicationServices.DTOs;

namespace ProjectStructure.ConsoleClient.Extensions
{
    public static class UserSummaryDTOExtensions
    {
        public static string ToFormattedString(this UserSummaryDTO dto) =>
            $"User: {dto.User.FirstName} {dto.User.LastName}" +
            $"\n\tLast project: {dto.LastProject?.Name}" +
            $"\n\tLast project tasks count: {dto.LastProjectTaskCount.ToString()}" +
            $"\n\tUnfinished tasks count: {dto.UnfinishedTasksCount.ToString()}" +
            $"\n\tLongest unfinished task: {dto.LongestFinishedTask?.Name}";
    }
}
