﻿using System;
using System.Threading.Tasks;
using ProjectStructure.ConsoleClient.Interfaces;
using ProjectStructure.ConsoleClient.Services;

namespace ProjectStructure.ConsoleClient
{
    static class Program
    {
        public const string AppTitle = "BSA 2021 Task 1";

        static async Task Main()
        {
            Console.Title = AppTitle;

            var tasks = DI.GetService<IBackgroundTasks>();
            await tasks.StartAsync();

            var app = DI.GetService<IAsyncProgram>();
            await app.RunAsync();

            await tasks.StopAsync();
        }
    }
}
