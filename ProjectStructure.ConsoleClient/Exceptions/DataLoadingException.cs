﻿using System;
using System.Net.Http;

namespace ProjectStructure.ConsoleClient.Exceptions
{
    public class DataLoadingException : Exception
    {
        public DataLoadingException(string message, HttpRequestException? httpException = default)
            : base(message, httpException) {}
    }
}
