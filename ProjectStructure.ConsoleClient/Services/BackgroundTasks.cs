﻿using System;
using System.Threading.Tasks;
using System.Timers;

namespace ProjectStructure.ConsoleClient.Services
{
    public class BackgroundTasks : IBackgroundTasks
    {
        private const int MarkTaskDelay = 1000;
        private const int MarkTaskPeriod = 3000;

        private readonly IDataProcessor _dataProcessor;

        private Timer _markTaskTimer = default!;

        private bool _started;

        public BackgroundTasks(IDataProcessor dataProcessor)
        {
            _dataProcessor = dataProcessor;
        }

        public Task StartAsync()
        {
            if (_started)
                return Task.CompletedTask;

            _markTaskTimer = new Timer
            {
                Interval = MarkTaskPeriod,
                AutoReset = true,
                Enabled = true
            };

            _markTaskTimer.Elapsed += OnMarkTaskTimerElapsed;

            _started = true;

            return Task.CompletedTask;
        }

        public Task StopAsync()
        {
            if(!_started)
                return Task.CompletedTask;

            _markTaskTimer.Elapsed -= OnMarkTaskTimerElapsed;
            _markTaskTimer.Dispose();

            _started = false;

            return Task.CompletedTask;
        }

        private async void OnMarkTaskTimerElapsed(object sender, ElapsedEventArgs args)
        {
            try
            {
                var markedTaskId = await _dataProcessor.MarkRandomTaskWithDelay(MarkTaskDelay);

                Console.WriteLine();
                Console.WriteLine($"Marked task with id {markedTaskId.ToString()} as finished");
                Console.WriteLine();
            }
            catch (Exception e)
            {
                Console.WriteLine();
                Console.WriteLine("Couldn't mark random task");
                Console.WriteLine($"Error message: {e.Message}");
                Console.WriteLine();
            }
        }
    }
}
