﻿using System;
using System.Threading.Tasks;

namespace ProjectStructure.ConsoleClient.Services
{
    public interface IDataLoader : IDisposable
    {
        public Task<T?> GetDeserializedAsync<T>(string uri);
    }
}
