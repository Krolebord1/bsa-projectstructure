﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructure.ConsoleClient.Interfaces;

namespace ProjectStructure.ConsoleClient.Programs
{
    public class NavigatorProgram : IAsyncProgram
    {
        private readonly List<NavigatorEntry> _entries;

        public NavigatorProgram(IEnumerable<KeyValuePair<string, IAsyncProgram>> programs)
        {
            _entries = programs
                .Select((kvp, index) => new NavigatorEntry(index, kvp.Key, kvp.Value))
                .ToList();
        }

        public async Task RunAsync()
        {
            while(true)
            {
                Console.Clear();
                WriteCatalog();

                var line = Console.ReadLine();

                if(line == null)
                    continue;

                string argument = line.ToLower();

                if(string.IsNullOrWhiteSpace(argument))
                    break;

                var entry = int.TryParse(argument, out int index)
                    ? _entries.Find(x => x.Index == index-1)
                    : _entries.Find(x => x.Path.ToLower() == argument);

                if (entry == null)
                    continue;

                Console.Title = entry.Path;
                Console.Clear();

                await entry.Program.RunAsync();

                Console.Title = Program.AppTitle;
            }
        }

        private void WriteCatalog()
        {
            Console.WriteLine("Select action: ");
            foreach (NavigatorEntry entry in _entries)
                Console.WriteLine($"{entry.Index+1}. {entry.Path}");

            Console.WriteLine("Or press enter to go back.");
        }

        private class NavigatorEntry
        {
            public int Index { get; }
            public string Path { get; }
            public IAsyncProgram Program { get; }

            public NavigatorEntry(int index, string path, IAsyncProgram program)
            {
                Index = index;
                Path = path;
                Program = program;
            }
        }
    }
}
