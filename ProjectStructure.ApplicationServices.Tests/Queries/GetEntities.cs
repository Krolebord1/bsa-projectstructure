using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using OneOf;
using OneOf.Types;
using ProjectStructure.ApplicationServices.DTOs;
using ProjectStructure.ApplicationServices.DTOs.Projects;
using ProjectStructure.ApplicationServices.DTOs.Tasks;
using ProjectStructure.ApplicationServices.DTOs.Teams;
using ProjectStructure.ApplicationServices.Handlers.Projects;
using ProjectStructure.ApplicationServices.Handlers.Tasks;
using ProjectStructure.ApplicationServices.Handlers.Teams;
using ProjectStructure.ApplicationServices.Handlers.Users;
using ProjectStructure.ApplicationServices.MapperProfiles;
using ProjectStructure.ApplicationServices.Queries;
using ProjectStructure.Domain.Entities;
using ProjectStructure.Domain.Interfaces;
using ProjectStructure.Domain.Interfaces.Repositories;
using ProjectStructure.Tests.Abstractions;
using ProjectStructure.Tests.Abstractions.MockServices;
using Xunit;

namespace ProjectStructure.ApplicationServices.Tests.Queries
{
    public class GetEntities
    {
        private readonly IMapper _mapper;

        public GetEntities()
        {
            _mapper = GetMapper();
        }

        private static List<User> GetUsersList() => new()
        {
            new User {Id = 1, FirstName = "First"},
            new User {Id = 2, FirstName = "Second"},
            new User {Id = 3, FirstName = "Not First"},
            new User {Id = 43, FirstName = "43"}
        };

        private static List<Project> GetProjectsList() => new()
        {
            new Project {Id = 1, Name = "First"},
            new Project {Id = 2, Name = "Second"},
            new Project {Id = 3, Name = "Not First"},
            new Project {Id = 43, Name = "43"}
        };

        private static List<Team> GetTeamsList() => new()
        {
            new Team {Id = 1, Name = "First"},
            new Team {Id = 2, Name = "Second"},
            new Team {Id = 3, Name = "Not First"},
            new Team {Id = 43, Name = "43"}
        };

        private static List<UserTask> GetTasksList() => new()
        {
            new UserTask {Id = 1, Name = "First"},
            new UserTask {Id = 2, Name = "Second"},
            new UserTask {Id = 3, Name = "Not First"},
            new UserTask {Id = 43, Name = "43"}
        };

        public class GetEntityByIdData<TEntity, TReadDTO> : TheoryDataBase
            where TEntity : class, IEntity
        {
            public readonly List<TEntity> entities;
            public readonly OneOf<TReadDTO, NotFound> expectedResult;
            public readonly int id;
            public readonly Type handlerType;

            public GetEntityByIdData(List<TEntity> entities, OneOf<TReadDTO, NotFound> expectedResult, int id, Type handlerType, string description = "#") :
                base(description)
            {
                this.entities = entities;
                this.expectedResult = expectedResult;
                this.id = id;
                this.handlerType = handlerType;
            }
        }

        public static IEnumerable<object[]> GetEntityByIdQuery_Data()
        {
            var mapper = GetMapper();

            // Users
            yield return new object[] { new GetEntityByIdData<User, UserReadDTO>(
                entities: GetUsersList(),
                expectedResult: mapper.Map<UserReadDTO>(new User {Id = 3, FirstName = "Not First"}),
                id: 3,
                handlerType: typeof(GetUserByIdHandler),
                description: "when id is correct"
            ) };

            yield return new object[] { new GetEntityByIdData<User, UserReadDTO>(
                entities: GetUsersList(),
                expectedResult: new NotFound(),
                id: 100,
                handlerType: typeof(GetUserByIdHandler),
                description: "when id is incorrect"
            ) };

            // Projects
            yield return new object[] { new GetEntityByIdData<Project, ProjectReadDTO>(
                entities: GetProjectsList(),
                expectedResult: mapper.Map<ProjectReadDTO>(new Project {Id = 3, Name = "Not First"}),
                id: 3,
                handlerType: typeof(GetProjectByIdHandler),
                description: "when id is correct"
            ) };

            yield return new object[] { new GetEntityByIdData<Project, ProjectReadDTO>(
                entities: GetProjectsList(),
                expectedResult: new NotFound(),
                id: 100,
                handlerType: typeof(GetProjectByIdHandler),
                description: "when id is incorrect"
            ) };

            // Teams
            yield return new object[] { new GetEntityByIdData<Team, TeamReadDTO>(
                entities: GetTeamsList(),
                expectedResult: mapper.Map<TeamReadDTO>(new Team {Id = 3, Name = "Not First"}),
                id: 3,
                handlerType: typeof(GetTeamByIdHandler),
                description: "when id is correct"
            ) };

            yield return new object[] { new GetEntityByIdData<Team, TeamReadDTO>(
                entities: GetTeamsList(),
                expectedResult: new NotFound(),
                id: 100,
                handlerType: typeof(GetTeamByIdHandler),
                description: "when id is incorrect"
            ) };

            // Tasks
            yield return new object[] { new GetEntityByIdData<UserTask, TaskReadDTO>(
                entities: GetTasksList(),
                expectedResult: mapper.Map<TaskReadDTO>(new UserTask {Id = 3, Name = "Not First"}),
                id: 3,
                handlerType: typeof(GetTaskByIdHandler),
                description: "when id is correct"
            ) };

            yield return new object[] { new GetEntityByIdData<UserTask, TaskReadDTO>(
                entities: GetTasksList(),
                expectedResult: new NotFound(),
                id: 100,
                handlerType: typeof(GetTaskByIdHandler),
                description: "when id is incorrect"
            ) };
        }

        [Theory]
        [MemberData(nameof(GetEntityByIdQuery_Data))]
        public async Task GetEntityByIdQuery<TEntity, TReadDTO>(GetEntityByIdData<TEntity, TReadDTO> data)
            where TEntity : class, IEntity
        {
            IReadRepository<TEntity> repository = new MockRepository<TEntity>(data.entities);

            var query = new GetEntityByIdQuery<TReadDTO>(data.id);
            var handler = (IRequestHandler<GetEntityByIdQuery<TReadDTO>, OneOf<TReadDTO, NotFound>>)Activator.CreateInstance(data.handlerType, repository, _mapper)!;

            var result = await handler.Handle(query, CancellationToken.None);

            Assert.Equal(result, data.expectedResult);
        }

        public class GetEntitiesData<TEntity, TReadDTO> : TheoryDataBase
            where TEntity : class, IEntity
        {
            public readonly List<TEntity> entities;
            public readonly IEnumerable<TReadDTO> expectedResult;
            public readonly Type handlerType;

            public GetEntitiesData(List<TEntity> entities, IEnumerable<TReadDTO> expectedResult, Type handlerType, string description = "#") :
                base(description)
            {
                this.entities = entities;
                this.expectedResult = expectedResult;
                this.handlerType = handlerType;
            }
        }

        public static IEnumerable<object[]> GetEntitiesQuery_Data()
        {
            var mapper = GetMapper();

            // Users
            var users = GetUsersList();

            yield return new object[] { new GetEntitiesData<User, UserReadDTO>(
                entities: users,
                expectedResult: mapper.Map<IEnumerable<UserReadDTO>>(users),
                handlerType: typeof(GetUsersHandler),
                description: "not empty list"
            ) };

            yield return new object[] { new GetEntitiesData<User, UserReadDTO>(
                entities: new List<User>(),
                expectedResult: Enumerable.Empty<UserReadDTO>(),
                handlerType: typeof(GetUsersHandler),
                description: "empty list"
            ) };

            // Projects
            var projects = GetProjectsList();

            yield return new object[] { new GetEntitiesData<Project, ProjectReadDTO>(
                entities: projects,
                expectedResult: mapper.Map<IEnumerable<ProjectReadDTO>>(projects),
                handlerType: typeof(GetProjectsHandler),
                description: "not empty list"
            ) };

            yield return new object[] { new GetEntitiesData<Project, ProjectReadDTO>(
                entities: new List<Project>(),
                expectedResult: Enumerable.Empty<ProjectReadDTO>(),
                handlerType: typeof(GetProjectsHandler),
                description: "empty list"
            ) };

            // Teams
            var teams = GetTeamsList();

            yield return new object[] { new GetEntitiesData<Team, TeamReadDTO>(
                entities: teams,
                expectedResult: mapper.Map<IEnumerable<TeamReadDTO>>(teams),
                handlerType: typeof(GetTeamsHandler),
                description: "not empty list"
            ) };

            yield return new object[] { new GetEntitiesData<Team, TeamReadDTO>(
                entities: new List<Team>(),
                expectedResult: Enumerable.Empty<TeamReadDTO>(),
                handlerType: typeof(GetTeamsHandler),
                description: "empty list"
            ) };

            // Tasks
            var tasks = GetTasksList();

            yield return new object[] { new GetEntitiesData<UserTask, TaskReadDTO>(
                entities: tasks,
                expectedResult: mapper.Map<IEnumerable<TaskReadDTO>>(tasks),
                handlerType: typeof(GetTasksHandler),
                description: "not empty list"
            ) };

            yield return new object[] { new GetEntitiesData<UserTask, TaskReadDTO>(
                entities: new List<UserTask>(),
                expectedResult: Enumerable.Empty<TaskReadDTO>(),
                handlerType: typeof(GetTasksHandler),
                description: "empty list"
            ) };
        }

        [Theory]
        [MemberData(nameof(GetEntitiesQuery_Data))]
        public async Task GetEntitiesQuery<TEntity, TReadDTO>(GetEntitiesData<TEntity, TReadDTO> data)
            where TEntity : class, IEntity
        {
            IReadRepository<TEntity> repository = new MockRepository<TEntity>(data.entities);

            var query = new GetEntitiesQuery<TReadDTO>();
            var handler = (IRequestHandler<GetEntitiesQuery<TReadDTO>, IEnumerable<TReadDTO>>)Activator.CreateInstance(data.handlerType, repository, _mapper)!;

            var result = await handler.Handle(query, CancellationToken.None);

            Assert.Equal(result, data.expectedResult);
        }

        private static IMapper GetMapper()
        {
            var configuration = new MapperConfiguration(expression =>
            {
                expression.AddProfile(new UsersProfile());
                expression.AddProfile(new ProjectsProfile());
                expression.AddProfile(new TeamsProfile());
                expression.AddProfile(new TasksProfile());
            });

            return configuration.CreateMapper();
        }
    }
}
